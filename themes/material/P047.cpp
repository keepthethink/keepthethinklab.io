#include<iostream>
using namespace std;

const int MAXN = 10000 + 5;

int num[MAXN << 2], sum[MAXN << 2], add[MAXN << 2];
int n, m;

void build(int p, int l, int r) {
    add[p] = 0;
    if(l == r) {
        sum[p] = 1;
    }

    int mid = (l + r) >> 1;
    build(p << 1, l, mid);
    build(p << 1 | 1, mid + 1, r);
    sum[p] = sum[p << 1] + sum[p << 1 | 1];
}

void f(int p, int l, int r, int k) {
    add[p] += k;
    sum[p] += k * (r - l + 1);
}

void pushdown(int p, int l, int r) {
    int mid = (l + r) >> 1;
    f(p << 1, l, mid, add[p]);
    f(p << 1 | 1, mid + 1, r, add[p]);
    add[p] = 0;
}

void modify(int p, int l, int r, int i, int j, int k) {
    if(i <= l && j >= r) {
        f(p, l, r, k);
        return;
    }

    pushdown(p, l, r);
    int mid = (l + r) >> 1;
    if(i <= mid) {
        modify(p << 1, l, mid, i, j, k);
    }

    if(j > mid) {
        modify(p << 1 | 1, mid + 1, r, i, j, k);
    }
}

int query(int p, int l, int r, int i, int j) {
    int ans = 0;
    if(i <= l && j >= r) {
        return sum[p];
    }

    pushdown(p, l, r);
    int mid = (l + r) >> 1;
    if(i <= mid) {
        ans += query(p << 1, l, mid, i, j);
    }

    if(j > mid) {
        ans += query(p << 1 | 1, mid + 1, r, i, j);
    }

    return ans;
}

int main() {
    cin >> n >> m;

    for(int i = 1; i <= m; i++) {
        int x, y;

        cin >> x >> y;
        modify(1, 1, n, x, y, -1);
    }

    cout << query(1, 1, n, 1, n) << endl;

    return 0;
}